const sendButton = document.querySelector('.send')
const api = 'http://treinamento-ajax-api.herokuapp.com/messages/'
const messagesHistory = document.querySelector('.messages')
const text = document.querySelector('#text-message')

const createMessage = (message) => {
  const li = document.createElement('li')
  li.classList.add('message')
  li.innerHTML = `
    <div class="content">
    <p>${message.content}</p>
    <h2> - ${message.author}</h2>
    </div>
    <div class="buttons">
      <button class="edit" onclick="showEditOptions(this)">EDITAR</button>
      <button onclick="delMessage(this, ${message.id}) "class="del">DELETAR</button>
    </div>
    <div class="edit-options" hidden>
      <input class="text-edit">
      <input type="button" value="SEND" class="send-edit" onclick="editMessage(this, ${message.id})">
    </div>
  `
  messagesHistory.appendChild(li)
}

const getMessages = () => {
  fetch(api)
    .then(res => res.json())
    .then(messages => {
      messages.forEach(message => {
        createMessage(message)
      })
    })
    .catch((err) => {
      alert("Ocorreu um erro")
    })
}

const showEditOptions = (elemento) => {
  const parent = elemento.parentNode.parentNode
  parent.querySelector(".edit-options").toggleAttribute("hidden")
}

const editMessage = (elemento, id) => {
  const parent = elemento.parentNode.parentNode
  const msg = elemento.parentNode.children[0]
  const body = {
    message: {
      content: elemento.previousElementSibling.value,
      author: "Gustavo Vasquez"
    }
  }
  const config = {
    method: "PUT",
    body: JSON.stringify(body),
    headers: { "Content-Type": "application/json" }
  }
  fetch(api + id, config)
    .then(response => response.json())
    .then(message => {
      parent.querySelector("h2").innerText = `${message.author}`
      parent.querySelector(".content p").innerText = message.content
    })
    .catch(() => {
      alert("Aconteceu um erro")
    })
  parent.querySelector(".edit-options").toggleAttribute("hidden")
  msg.value = ''
}

const postMessage = () => {
  const body = {
    message: {
      content: text.value,
      author: "Gustavo Vasquez"
    }
  }
  const config = {
    method: 'POST',
    body: JSON.stringify(body),
    headers: { "Content-Type": "application/json" }
  }
  fetch(api, config)
    .then(response => response.json())
    .then(message => {
      text.value !== '' && createMessage(message)
    })
    .catch(() => {
      alert("Aconteceu um erro")
    })
}

const delMessage = (element, id) => {
  const config = {
    method: "DELETE"
  }
  fetch(api + id, config)
    .then(() => {
      element.parentNode.parentNode.remove()
    })
    .catch(() => {
      alert("Aconteceu um erro")
    })
}

sendButton.addEventListener("click", () => postMessage())
getMessages()